# Git Seminar Übungsaufgaben



### On Your Local Machine

![Quick Start](/images/quickstart.gif)

- Clone this repository
- Go into the folder you want to solve an exercise in
- Run the `setup.sh` script
- Consult the README.md in that folder to get a description of the exercise

## Suggested Learning Path

If you are coming to this repository for some basic Git knowledge, we recommend going through the exercises in the following order.

- [Basic Commits](./exercises/basic-commits/README.md)
- [Basic Staging](./exercises/basic-staging/README.md)
- [Investigation](./exercises/investigation/README.md)
- [Basic Branching](./exercises/basic-branching/README.md)
- [Fast Forward Merge](./exercises/ff-merge/README.md)
- [3 way Merge](./exercises/3-way-merge/README.md)
- [Merge Mergesort](./exercises/merge-mergesort/README.md)
- [Rebase Branch](./exercises/rebase-branch/README.md)
- [Basic Revert](./exercises/basic-revert/README.md)
- [Reset](./exercises/reset/README.md)
- [Basic Cleaning](./exercises/basic-cleaning/README.md)
- [Amend](./exercises/amend/README.md)
- [Reorder the History](./exercises/reorder-the-history/README.md)
- [Advanced Rebase Interactive](./exercises/advanced-rebase-interactive/README.md)
- [Rebase using autosquash](./exercises/rebase-interactive-autosquash/README.md)
- [Basic Stashing](./exercises/basic-stashing/README.md)

## Source
[Git Ketas](https://github.com/eficode-academy/git-katas)